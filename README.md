# README #

To execute the batch, you open an execute anonymous window in the Force.com Developer Console and paste the following:

// This is just an example, add as many field api names as you want, and map them to the new values you want to update on the account
Map<String, String> nameToValue = new Map<String, String>{
	'Enterprise_Account_Status__c' => 'Bronze'
};
Id batchId = Database.executeBatch(new RefreshAccounts(<insert query string>, nameToValue), 200);

<insert query string> = the query of accounts that you want updated. It could be any valid query 
	(ex: 'SELECT Id, Enterprise_Account_Status__c FROM Account WHERE RecordType.Name = \'Customer Account\'')
	Be SURE that you include ALL field from the map above in the query or you will receive an error. 
	I could have gotten the fields from the map myself, 
	but didn't want to risk the user adding the field and then my code adding it again. It could have been done that way though.
	I also though that the user could add only the filter criteria and I create the query, but I'm assuming the user will be 
	somewhat familiar with SOQL and this would be easier
		
One you are done editing, click execute and the batch will run in the background until all Accounts are updated.