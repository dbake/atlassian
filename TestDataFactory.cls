public with sharing class TestDataFactory {
  public static Account buildAccount(String name) {
    Account a = new Account(Name = name);
    return a;
  }

  public static Account createAccount(String name) {
    Account a = buildAccount(name);
    insert a;
    return a;
  }
}