@isTest
private class RefreshAccountsTest {
	@isTest static void updateToBronze() {
    List<Account> accounts =
        [SELECT Id FROM Account WHERE Enterprise_Account_Status__c = 'Bronze'];
    System.assert(accounts.isEmpty());
    Map<String, String> nameToValue = new Map<String, String>{
      'Enterprise_Account_Status__c' => 'Bronze'
    };
    Test.startTest();
		Id batchId = Database.executeBatch(new RefreshAccounts(
        'SELECT Id, Enterprise_Account_Status__c ' +
        'FROM Account ' +
        'WHERE RecordType.Name = \'Customer Account\'', nameToValue), 200);
    Test.stopTest();
    accounts =
        [SELECT Id FROM Account WHERE Enterprise_Account_Status__c = 'Bronze'];
    System.assertEquals(accounts.size(), 200);
	}

  @isTest static void updateToGold() {
    List<Account> accounts =
        [SELECT Id FROM Account WHERE Enterprise_Account_Status__c = 'Gold'];
    System.assert(accounts.isEmpty());
    Map<String, String> nameToValue = new Map<String, String>{
      'Enterprise_Account_Status__c' => 'Gold'
    };
    Test.startTest();
    Id batchId = Database.executeBatch(new RefreshAccounts(
        'SELECT Id, Enterprise_Account_Status__c ' +
        'FROM Account ' +
        'WHERE Gold_Account__c = true', nameToValue), 200);
    Test.stopTest();
    accounts =
        [SELECT Id FROM Account WHERE Enterprise_Account_Status__c = 'Gold'];
    System.assertEquals(accounts.size(), 100);
  }

	@testSetup static void initData() {
		List<Account> accounts = new List<Account>();
    Schema.RecordTypeInfo customer =
        Schema.SObjectType.Account.getRecordTypeInfosByName().get(
            'Customer Account');


    for (Integer i = 0; i < 200; i++) {
      Account a = TestDataFactory.buildAccount('Test Account ' + i);
      a.RecordTypeId = customer.getRecordTypeId();
      if (Math.mod(i, 2) == 0) {
        a.Gold_Account__c = true;
      }
      accounts.add(a);
    }

    insert accounts;
	}
}