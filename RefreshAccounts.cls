global class RefreshAccounts implements Database.Batchable<sObject> {
	String query;
	Map<String, String> fieldNameToNewValue;

	global RefreshAccounts(String query, Map<String, String> nameToValue) {
		this.query = query;
		this.fieldNameToNewValue = nameToValue;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<sObject> scope) {
  	if (scope.isEmpty()) { return; }
  	if (fieldNameToNewValue.isEmpty()) { return; }

  	List<Account> accounts = (List<Account>)scope;

  	for (Account a : accounts) {
  		for (String fieldName : fieldNameToNewValue.keySet()) {
  			a.put(fieldName, fieldNameToNewValue.get(fieldName));
  		}
  	}

  	update accounts;
  	// Didn't put this in a try catch or Database.update with a false all or
  	// nothing, because
	}

	global void finish(Database.BatchableContext BC) {}
}